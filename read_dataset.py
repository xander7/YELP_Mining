from models import DataSet
from models import Business
from models import Review
from models import User
from models import Checkin
from models import Tip
import sys
import json
import decimal
from datetime import datetime


def iterate_file(model_name, shortcircuit=True, status_frequency=10000):
    i = 0
    jsonfilename = model_name.lower() + ".json"
    with open(jsonfilename) as jfile:
        for line in jfile:
            i += 1
            yield json.loads(line)
            if i % status_frequency == 0:
                print("Status >>> %s: %d" % (jsonfilename, i))
                if shortcircuit and i == 10000:
                    raise StopIteration()


def read_businesses(main_folder, file_name_root):
    businesses_dict = dict()
    for bdata in iterate_file(main_folder + file_name_root + "business", shortcircuit=False):
        business = Business()
        business.business_id = bdata['business_id']
        business.name = bdata['name']
        business.neighborhoods = read_list(bdata['neighborhoods'])
        business.full_address = bdata['full_address']
        business.city = bdata['city']
        business.state = bdata['state']
        business.latitude = bdata['latitude']
        business.longitude = bdata['longitude']
        business.stars = decimal.Decimal(bdata.get('stars', 0))
        business.review_count = int(bdata['review_count'])
        business.categories = read_list(bdata['categories'])
        business.is_open = True if bdata['open'] == True else False
        business.hours = read_hours(bdata['hours'])
        business.attributes = read_attributes(bdata['attributes'])
        businesses_dict[business.business_id] = business

    return businesses_dict


def read_list(hood_jarray): #reads lists
    neighborhoods = list()
    for hood in hood_jarray:
        neighborhoods.append(hood)

    return neighborhoods


def read_categories(cat_jarray):
    categories = list()
    for name in cat_jarray:
        categories.append(name)

    return categories


def read_hours(hours_object):
    hours = dict()
    for day in hours_object:
        day_schedule = hours_object[day]
        for item in day_schedule:
            hours[day + "." + item] = day_schedule[item]

    return hours


def read_attributes(attributes_object):
    attributes = dict()
    for first_level in attributes_object:
        first_level_obj = attributes_object[first_level]
        if isinstance(first_level_obj, dict):
            for second_level in first_level_obj:
                attributes[first_level + "." + second_level] = first_level_obj[second_level]
        else:
            attributes[first_level] = attributes_object[first_level]

    return attributes


def read_reviews(main_folder, file_name_root):
    reviews_list=list()
    for rdata in iterate_file(main_folder + file_name_root +"review", shortcircuit=False):
        rev = Review()
        rev.business_id = rdata['business_id']
        rev.user_id = rdata['user_id']
        rev.stars = int(rdata.get('stars', 0))
        rev.text = rdata['text']
        rev.date = datetime.strptime(rdata['date'], "%Y-%m-%d")
        rev.useful_votes = int(rdata['votes']['useful'])
        rev.funny_votes = int(rdata['votes']['funny'])
        rev.cool_votes = int(rdata['votes']['cool'])
        reviews_list.append(rev)
    return reviews_list


def read_users(main_folder, file_name_root):
    users_dict = dict()
    for udata in iterate_file(main_folder + file_name_root + "user", shortcircuit=False):
        user = User()
        user.user_id = udata['user_id']
        user.name = udata['name']
        user.review_count = int(udata['review_count'])
        user.average_stars = decimal.Decimal(udata.get('average_stars', 0))
        user.useful_votes = int(udata['votes']['useful'])
        user.funny_votes = int(udata['votes']['funny'])
        user.cool_votes = int(udata['votes']['cool'])
        user.friends = read_list(udata['friends'])
        user.elite = read_list(udata['elite'])
        user.yelping_since = datetime.strptime(udata['yelping_since'], "%Y-%m") #don't know yet
        user.compliments= read_attributes(udata['compliments'])
        user.fans=int(udata['fans'])
        users_dict[user.user_id] = user

    return users_dict


def read_checkins(main_folder, file_name_root):
    checkins_list= list()
    for cdata in iterate_file(main_folder + file_name_root + "checkin", shortcircuit=False):
        checkin = Checkin()
        checkin.business_id = cdata['business_id']
        checkin.checkin_info=read_attributes(cdata['checkin_info'])
        checkins_list.append(checkin)
    return checkins_list


def read_tips(main_folder, file_name_root):
    tips_lists=list()
    for tdata in iterate_file(main_folder + file_name_root +"tip", shortcircuit=False):
        tip = Tip()
        tip.business_id = tdata['business_id']
        tip.text = tdata['text']
        tip.user_id = tdata['user_id']
        tip.date = datetime.strptime(tdata['date'], "%Y-%m-%d")
        tip.likes = int(tdata['likes'])

        tips_lists.append(tip)
    return tips_lists


def read_dataset(main_folder, file_name_root):
    ds = DataSet()
    ds.businesses = read_businesses(main_folder, file_name_root)
    ds.users = read_users(main_folder, file_name_root)
    ds.checkins = read_checkins(main_folder, file_name_root)
    ds.reviews = read_reviews(main_folder, file_name_root)
    ds.tips = read_tips(main_folder, file_name_root)

    return ds

if __name__ == "__main__":
    print("Reading dataset from folder: " + sys.argv[1])
    read_dataset(sys.argv[1], sys.argv[2])
    print("Done.")
