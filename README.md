# Yelp Mining

> This code computes minimal features and features selection on the YELP dataset and allow to predict stars rating
> given by users to businesses. In order to do so it makes available an own implementation of vanilla gradient descend Matrix Factorization and the sci-kit version of Support Vector Regression.

The YELP data should be downloaded from https://www.yelp.com/dataset_challenge/dataset and extracted in the 'data/json/' folder. Five json files are expected. Please keep the original names of the files:
 - yelp_academic_dataset_business.json
 - yelp_academic_dataset_user.json
 - yelp_academic_dataset_checkin.json
 - yelp_academic_dataset_review.json
 - yelp_academic_dataset_tip.json

Pre-requisites to run the code is to have installed:
  - Python (>= 2.6 or >= 3.3),
  - NumPy (>= 1.6.1),
  - SciPy (>= 0.9).
  - Scikit-leanrn (0.18)

Alternatively you can run the Dockerfile that will create an image of Anaconda with all the necessary dependencies installed.
To run the Dockerfile and enter the bash:
 ```sh
$ docker build -t 'image_name' .
$ docker run -it 'image_name' bash
```

The code is composed of 8 .py files and can call 6 methods:
  - feature_extraction
  - data_analysis
  - check_cold_start
  - feature_selection
  - regression_SVR
  - regression_MF

Please execute the methods in the same order for the first time.

Inside the bash, type the following commands to read the files and extract the featurese:

```sh
$ python ./feature_extraction.py ./data/json/ yelp_academic_dataset_ ./data/csv/features.csv ./data/csv/users.csv ./data/csv/business.csv
```
where:
  - ./data/json is the input files folder
  - yelp_academic_dataset_ is the part of the name common to all the input json files (do not change)
  - ./data/csv/features.csv is the path and the name of the output file
  - ./data/csv/users_dict.csv is the path and name of the output file containing the user dictionary (id-history length), used for the matrix factorization
  - ./data/csv/businesses_dict.csv is the path and name of the output file containing the business dictionary (id-history length), used for the matrix factorization

To analyze the data:
Plots histograms and box plots for each feature
```sh
$ python ./data_analysis.py ./data/csv/features.csv ./data/analysis/ True/False
```
where:
  - ./data/csv/features.csv is the path and the name of the input file (the dataset to be analyzed)
  - ./data/analysis/ is the folder where the results of the analysis are stored
  - write 'True' to plot the scatter matrix else write 'False' (for big datasets this can take some time and requires additional resources)

Counts the numeber of Cold Start Users and Businesses
```sh
$ python ./check_cold_start.py ./data/json/ yelp_academic_dataset_
```
where:
  - ./data/json is the input files folder
  - yelp_academic_dataset_ is the part of the name common to all the input json files (do not change)
  
This method is to perform a feature selection. Univariate feature selection based on f-measure
  ```sh
$ python ./feature_selection.py ./data/csv/features.csv [0,36] 30
```
where:
  - ./data/csv/features.csv is the path and the name of the input file
  - Features [0,36] are the indexes of users and businesses and therefore skipped by the analysis
  - 30 is the number of best features to select for the regression 

Computes Support Vector Regression for Star Rating prediction 
```sh
$ python ./regression_SVR.py ./data/csv/features.csv [0,36,60,61,62,63,64] 30 5
```
where:
  - ./data/csv/features.csv is the path and the name of the input file
  - [0,36,60,61,62,63,64] are the features to be skipped. Features [0,36] are the indexes of users and businesses and therefore skipped by the analysis. The others are the Review Features that should not be used for Review prediction
- 30 is the number of best features to select for the regression   
- 5 is the number of folds for the cross validation 

Computes Matrix Factorization for Star Rating prediction
```sh
python ./regression_MF.py ./data/csv/features.csv ./data/csv/users_dictionary.csv ./data/csv/businesses_dictionary.csv
```
where:
  - ./data/csv/features.csv is the path and the name of the input file
  - ./data/csv/users_dict.csv is the path and name to the file containing the user dictionary (id-history length), used for the matrix factorization
  - ./data/csv/businesses_dict.csv is the path and name to the file containing the business dictionary (id-history length), used for the matrix factorization