import sys
import read_dataset as rd
from datetime import datetime
from models import Features
from models import Pattern
import csv


def check_if_int(s):
    try:
        int(s)
        return True
    except ValueError:
        return False


def get_postal_code(address):
    fields = str(address).split(" ")
    if len(fields) <= 1: return None
    else:
        if check_if_int(fields[len(fields) - 1]): return fields[len(fields) - 1]
        elif check_if_int(fields[0]): return fields[0]
        else: return None


def get_head_word(word):

    #TODO use the package for stemming

    fields = str(word).split(" ")
    if len(fields) > 1: return fields[len(fields) - 1]
    else: return None


def get_time(input_date):
    date = datetime(input_date)
    return date.timestamp


def get_year(input_date):
    date = datetime(input_date)
    return date.year


def get_part_of_day(input_date):
    date = datetime(input_date)
    hour = date.hour
    if (hour >= 5) and (hour <= 13): return 0
    elif (hour > 13) and (hour <= 19): return 1
    else: return 2


def get_day(input_date):
    date = datetime(input_date)
    return date.weekday()


def create_vector_from_list(items_list, items_dictionary, is_binary):
    items_vector = list()
    for item in items_dictionary: items_vector.append(0)

    for item in items_list:
        index = items_dictionary[item] - 1
        if is_binary: items_vector[index] = 1
        else: items_vector[index] = items_list[item]

    return items_vector


def create_vector_from_hours_dictionary(items_dictionary, items_universe):
    items_vector = list()
    for item in items_universe: items_vector.append(-1)

    for item in items_dictionary:
        index = items_universe[item] - 1
        items_vector[index] = str(items_dictionary[item]).split(":")[0]

    return items_vector


def extract_review_features(review):
    review_features = list()

    # TODO preprocess text: tokenisation, stop-word removal, stemming
    # TODO BOW vector of the text
    # TODO topic of the text (LDA)
    # TODO number of verbs, nouns, adjectives in the text
    # TODO sentiment of the text
    # TODO word2vec applied to the text

    # length of the text
    #text = str(review.text)
    #review_features.append(len(review.text))

    # words in the text (simplified)
    #review_features.append(text.count(" "))

    # sentences in the text (simplified)
    #review_features.append(len(text.split(".")))

    # time of the review (unix time in milliseconds)
    #review_features.append(review.date.timestamp())

    # part of day of review (0 = morning, 1 = afternoon, 2 = night)
    #review_features.append(get_part_of_day(review.date))

    # day of review (0 = monday ... 6 = sunday)
    review_features.append(review.date.weekday())

    # cool, funny, useful, total votes
    # TODO handle outliers in review scores
    review_features.append(review.cool_votes)
    review_features.append(review.funny_votes)
    review_features.append(review.useful_votes)
    review_features.append(review.cool_votes + review.funny_votes + review.useful_votes)

    # TODO normalize the review rating by the avg rating of the user?

    return review_features


def extract_business_features(business, head_words, post_codes, cities, states, hours):
    business_features = list()
    business_features.append(business.business_id)

    # last word of the name as the main category of the business
    head_word = get_head_word(business.name)
    if head_word is None: business_features.append(0)
    else: business_features.append(head_words[head_word])

    # number of neighbors
    business_features.append(len(business.neighborhoods))

    # post code
    post_code = get_postal_code(business.full_address)
    if post_code is None: business_features.append(0)
    else: business_features.append(post_codes[post_code])

    #city
    city = business.city
    if city is None or city == "" or city == " ": business_features.append(0)
    else: business_features.append(cities[city])

    # state
    state = business.state
    if state is None or city == "" or city == " ": business_features.append(0)
    else: business_features.append(states[state])

    # latitude and longitude
    business_features.append(business.latitude)
    business_features.append(business.longitude)

    # stars
    business_features.append(business.stars)

    # review count
    business_features.append(business.review_count)

    # is open
    if business.is_open is True: business_features.append(1)
    else: business_features.append(0)

    # categories
    # TODO sparse vector: aggregate categories (e.g. based on semantics, string similarity, etc.)
    business_features.append(len(business.categories))

    # hours
    # TODO count number of open hours per day, also total/rate of open hours
    hours_vector = create_vector_from_hours_dictionary(business.hours, hours)
    for value in hours_vector:
        business_features.append(value)

    # attributes (subset chosen according to the dataset analysis)
    # TODO sparse vector: aggregate attributes (e.g. based on semantics, string similarity, etc.)
    accepts_credit_card = dict(business.attributes).get('Accepts Credit Cards')
    if accepts_credit_card is None: business_features.append(0)
    elif accepts_credit_card is False: business_features.append((0))
    else: business_features.append(1)

    alcohol = dict(business.attributes).get('Alcohol')
    if alcohol is None: business_features.append(0)
    elif alcohol == "none": business_features.append(0)
    elif alcohol == "red_wine": business_features.append(1)
    else: business_features.append(1)

    ambience_casual = dict(business.attributes).get('Ambience.casual')
    if ambience_casual is None: business_features.append(0)
    elif ambience_casual is False: business_features.append((0))
    else: business_features.append(1)

    good_for_groups = dict(business.attributes).get('Good For Groups')
    if good_for_groups is None: business_features.append(0)
    elif good_for_groups is False: business_features.append((0))
    else: business_features.append(1)

    good_for_kids = dict(business.attributes).get('Good for Kids')
    if good_for_kids is None: business_features.append(0)
    elif good_for_kids is False: business_features.append((0))
    else: business_features.append(1)

    has_tv = dict(business.attributes).get('Has TV')
    if has_tv is None: business_features.append(0)
    elif has_tv is False: business_features.append((0))
    else: business_features.append(1)

    outdoor = dict(business.attributes).get('Outdoor Seating')
    if outdoor is None: business_features.append(0)
    elif outdoor is False: business_features.append((0))
    else: business_features.append(1)

    parking = dict(business.attributes).get('Parking.lot')
    if parking is None: business_features.append(0)
    elif parking is False: business_features.append((0))
    else: business_features.append(1)

    price = dict(business.attributes).get('Price Range')
    if price is None: business_features.append(0)
    else: business_features.append(price)

    reservations = dict(business.attributes).get('Takes Reservations')
    if reservations is None: business_features.append(0)
    elif reservations is False: business_features.append((0))
    else: business_features.append(1)

    return business_features


def extract_user_features(user, compliments):
    user_features = list()
    user_features.append(user.user_id)
    user_features.append(len(user.name))

    # review count
    user_features.append(user.review_count)

    # user fans
    user_features.append(user.fans)

    # average stars
    user_features.append(user.average_stars)

    # compliments
    compliments_vector = create_vector_from_list(user.compliments, compliments, False)
    for value in compliments_vector:
        user_features.append(value)

    tot_of_compliments = 0
    tot_of_compliments_types = 0
    for compl in user.compliments:
        tot_of_compliments += user.compliments[compl]
        tot_of_compliments_types += 1

    user_features.append(tot_of_compliments)
    user_features.append(tot_of_compliments_types)

    # user elite
    user_features.append(len(user.elite))

    # yelping since
    user_features.append(user.yelping_since.year)

    # votes
    user_features.append(user.cool_votes)
    user_features.append(user.funny_votes)
    user_features.append(user.useful_votes)

    # friends
    user_features.append(len(user.friends))

    return user_features


def make_dictionary(input_list):
    dictionary = dict()

    value = 1;
    for elem in input_list:
        if elem not in dictionary:
            dictionary[elem] = value
            value += 1

    return dictionary


def make_business_head_words_dictionary(business_list):
    # put all the head words in a single list
    words_list = list()
    for business in business_list:
        head_word = get_head_word(business_list[business].name)
        if head_word is not None:
            words_list.append(head_word)

    # create the dictionary of the head words
    words_dict = make_dictionary(words_list)

    return words_dict


def make_business_post_code_dictionary(business_list):
    # put all the post codes in a single list
    codes_list = list()
    for business in business_list:
        code = get_postal_code(business_list[business].full_address)
        if code is not None:
            codes_list.append(code)

    # create the dictionary of the head words
    codes_dict = make_dictionary(codes_list)

    return codes_dict


def make_business_cities_dictionary(business_list):
    # put all the cities in a single list
    cities_list = list()
    for business in business_list:
        city = business_list[business].city
        if city is not None or city != "" or city != " ":
            cities_list.append(city)

    # create the dictionary of the cities
    cities_dict = make_dictionary(cities_list)

    return cities_dict


def make_business_states_dictionary(business_list):
    # put all the states in a single list
    states_list = list()
    for business in business_list:
        state = business_list[business].state
        if state is not None or state != "" or state != " ":
            states_list.append(state)

    # create the dictionary of the states
    states_dict = make_dictionary(states_list)

    return states_dict


def make_business_hours_dictionary(business_list):
    # put all the categories in a single list
    hours_list = list()
    for business in business_list:
        for hour in business_list[business].hours:
            hours_list.append(hour)

    # create the dictionary of the hours
    hours_dict = make_dictionary(hours_list)

    return hours_dict


def make_users_compliments_dictionary(users_list):
    # put all the compliments in a single list
    compliments_list = list()
    for user in users_list:
        for compliment in users_list[user].compliments:
            compliments_list.append(compliment)

    # create the dictionary of the compliments
    compliments_dict = make_dictionary(compliments_list)

    return compliments_dict


def extract_features(input_folder, file_name_root, output_dataset_file, output_usr_dict_file, output_bsn_dict_file):
    print("Reading dataset from folder: " + input_folder)
    dataset = rd.read_dataset(input_folder, file_name_root)

    total_review_user = dict()
    total_review_businesses = dict()

    # make dictionaries to create word vectors later (for categorical attributes)
    head_words = make_business_head_words_dictionary(dataset.businesses)
    post_codes = make_business_post_code_dictionary(dataset.businesses)
    cities = make_business_cities_dictionary(dataset.businesses)
    states = make_business_states_dictionary(dataset.businesses)
    hours = make_business_hours_dictionary(dataset.businesses)
    compliments = make_users_compliments_dictionary(dataset.users)

    # users
    print("Extracting user features")
    users_dict = dict()
    for user in dataset.users:
        users_dict[dataset.users[user].user_id] = extract_user_features(dataset.users[user], compliments)

    # businesses
    print("Extracting business features")
    businesses_dict = dict()
    for business in dataset.businesses:
        businesses_dict[dataset.businesses[business].business_id] = extract_business_features(dataset.businesses[business], head_words, post_codes, cities, states, hours)

    # reviews
    fout = open(output_dataset_file, 'w+', encoding='utf-8', newline='')
    csv_file = csv.writer(fout)
    first = True
    i = 0
    print("Extracting review features")
    for review in dataset.reviews:

        if review.business_id in total_review_businesses:
            total_review_businesses[review.business_id] += 1
        else: total_review_businesses[review.business_id] = 1

        if review.user_id in total_review_user:
            total_review_user[review.user_id] += 1
        else: total_review_user[review.user_id] = 1

        features = Features()
        features.review_features = extract_review_features(review)
        features.business_features = businesses_dict[review.business_id]
        features.user_features = users_dict[review.user_id]
        label = review.stars

        # fill the pattern
        pattern = Pattern()
        pattern.features = features
        pattern.label = label

        # print first line of the csv, only at the first iteration
        if first:
            first = False
            count_feat = [len(pattern.features.business_features), len(pattern.features.user_features), len(pattern.features.review_features)]
            name_feat = ['business', 'user', 'review']

            first_line = list()
            for feature_index in range(0,len(name_feat)):
                for count_index in range(0, count_feat[feature_index]):
                    first_line.append(name_feat[feature_index] + "_" + str(count_index))
            first_line.append("label")
            csv_file.writerow(first_line)

        # print the features for the current pattern
        pattern_line = list()
        pattern_line = pattern.features.business_features + pattern.features.user_features + pattern.features.review_features
        pattern_line.append(pattern.label)

        csv_file.writerow(pattern_line)

        # log
        i += 1
        if i % 10000 == 0:
            print("Processed reviews: " + str(i))

    fout.close()

    # additional files for matrix factorization
    with open(output_usr_dict_file, 'w+', encoding='utf-8', newline='') as csv_file:
        writer = csv.writer(csv_file)
        for key in total_review_user:
            writer.writerow([key, str(total_review_user[key])])

    with open(output_bsn_dict_file, 'w+', encoding='utf-8', newline='') as csv_file:
        writer = csv.writer(csv_file)
        for key in total_review_businesses:
            writer.writerow([key, str(total_review_businesses[key])])


if __name__ == "__main__":

    print("Extracting features")
    extract_features(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5])
    print("Done.")
