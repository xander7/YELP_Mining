import read_dataset as rd
import sys


def check_cold_start(input_folder, file_name_root):
    print("Assessing cold start in dataset: " + input_folder)
    dataset = rd.read_dataset(input_folder, file_name_root)

    # count the users and businesses over reviews
    print("Counting cold users and businesses")
    total_review_user = dict()
    total_review_businesses = dict()
    for review in dataset.reviews:

        if review.business_id in total_review_businesses:
            total_review_businesses[review.business_id] += 1
        else: total_review_businesses[review.business_id] = 1

        if review.user_id in total_review_user:
            total_review_user[review.user_id] += 1
        else: total_review_user[review.user_id] = 1

    # assess cold start
    cold_business = 0
    for cbusiness in total_review_businesses:
        if total_review_businesses[cbusiness] < 10: cold_business += 1

    cold_user = 0
    for cuser in total_review_user:
        if total_review_user[cuser] < 10: cold_user += 1

    # show result
    print("COLD START INFO:")
    print("Cold users: " + str(cold_user))
    print("Cold businesses: " + str(cold_business))


if __name__ == "__main__":
    check_cold_start(sys.argv[1], sys.argv[2])
    print("Done.")
