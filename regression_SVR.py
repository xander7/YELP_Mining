import sklearn.svm as svm
import sys, ast
from math import sqrt
from sklearn.cross_validation import train_test_split
from sklearn.grid_search import GridSearchCV
from sklearn.metrics import mean_squared_error
from sklearn.metrics.scorer import make_scorer
from feature_selection import feature_analysis


NUM_PATTERNS = 40000


def run_regression(input_file, columns_to_ignore, top_features, num_folds):

    # run feature selection (scaling is done within the feature selection after reading the dataset)
    print("Reading and selecting features from dataset: " + input_file)
    features, label = feature_analysis(input_file, columns_to_ignore, top_features)

    # reduce the number of patterns (for sake of time and testing)
    features = features[0:NUM_PATTERNS, :]
    label = label[0:NUM_PATTERNS]

    # split data in train and test
    print("Splitting data in train and test set")
    x_train, x_test, y_train, y_test = train_test_split(features, label, test_size=0.33, random_state=0)


    # set the parameters for grid search (using the train test)
    print("Tuning parameters via cross-validation")
    #tuned_parameters = [{'kernel': ['rbf'], 'gamma': [0.01, 0.1, 1], 'C': [0.1, 1, 10]}]
    tuned_parameters = [ {'kernel': ['linear'], 'C': [0.01, 0.1, 1, 10]}]

    # define the scoring criterion
    def RMSE(y_actual, y_predicted):
        rmse = sqrt(abs(mean_squared_error(y_actual, y_predicted)))
        return rmse
    score_rmse = make_scorer(RMSE, greater_is_better=False)
    scores = [score_rmse]

    # run the training (one iteration for each evaluation criterion, 1 in this case)
    for score in scores:
        print("# Tuning hyper-parameters for %s" % score)
        clf = GridSearchCV(svm.SVR(cache_size=2000), tuned_parameters, cv=ast.literal_eval(num_folds), scoring=score_rmse, verbose=10)
        clf.fit(x_train, y_train)

        # show results in command line
        print("\n\n")
        results_str = "SUMMARY\n"
        results_str += "The model is trained on the full train set and scores are computed on the full test set.\n"
        results_str += "best estimator:\t" + str(clf.best_estimator_) + "\n"
        results_str += "best parameters:\t" + str(clf.best_params_) + "\n"
        results_str += "best " + str(score) + ":\t" + str(abs(clf.score(x_test,y_test))) + "\n"
        results_str += "number of folds:\t " + str(clf.cv) + "\n"
        results_str += "grid scores:\t" + str(clf.grid_scores_)
        print(results_str)


if __name__ == "__main__":
    print("Running Support Vector Regression")
    run_regression(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4])
    print("Done")
